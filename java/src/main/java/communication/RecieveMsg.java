package communication;

import com.google.gson.JsonObject;


public class RecieveMsg {
	private String msgType;
	private Object data;
	private String gameId;
	private int gameTick;
	
//	@Deprecated
//	protected RecieveMsg(String msgType, Object data, String gameId, int gameTick){
//		this.msgType = msgType;
//		this.data = data;
//		this.gameId = gameId;
//		this.gameTick = gameTick;
//	}
//	
//	protected RecieveMsg(RecieveMsgWrapper rmw) throws UnknownMsgException{
//		this.msgType = rmw.getMsgType();
//		this.gameId = rmw.getGameId();
//		this.gameTick = rmw.getGameTick();
//		
////		this.data = RecieveMsg.dataTrans(this.msgType, rmw.getData());
//		this.data = rmw.getData();
//	}
	
	public String getMsgType(){
		return new String(msgType);
	}
	
	public Object getData(){
		return data;
	}
	
	public String getGameId(){
		return new String(gameId);
	}
	
	public int getGameTick(){
		return gameTick;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof RecieveMsg){
			RecieveMsg rm = (RecieveMsg) obj;
			return rm.getMsgType().equals(this.msgType);
		}else if(obj instanceof String){
			String msgType = (String) obj;
			return msgType.equals(this.msgType);
		}
		
		return false;
	}
	
//	private static Object dataTrans(String msgType, Object data) throws UnknownMsgException{
//		Gson gson = new Gson();
//		   if(msgType.equals("yourCar")){
//			   return gson.fromJson(data.toString(), YourCar.class);
//		   }else if(msgType.equals("gameInit")){
//			   return gson.fromJson(data.toString(), GameInit.class);
//		   }else if(msgType.equals("carPositions")){
//			   return gson.fromJson(data.toString(), ArrayList.class);
//		   }else if(msgType.equals("gameEnd")){
//			   return gson.fromJson(data.toString(), GameEnd.class);
//		   }else if(msgType.equals("crash")){
//			   return gson.fromJson(data.toString(), Crash.class);
//		   }else if(msgType.equals("spawn")){
//			   return gson.fromJson(data.toString(), Spawn.class);
//		   }else if(msgType.equals("lapFinished")){
//			   return gson.fromJson(data.toString(), LapFinished.class);
//		   }else if(msgType.equals("dnf")){
//			   return gson.fromJson(data.toString(), Dnf.class);
//		   }else if(msgType.equals("finish")){
//			   return gson.fromJson(data.toString(), Finish.class);
//		   }else if(msgType.equals("join")){
//			   return data;
//		   }else if( msgType.equals("gameStart") || msgType.equals("tornamentEnd") ){
//			   return null;
//		   }else{
//			   throw new UnknownMsgException();
//		   }
//	}
	
}
