package communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import com.google.gson.Gson;
import communication.msg.SendMsg;
import communication.msg.sendmsg.Join;

public class CommunicationDelegator {
	
//	private final Socket socket;
	
	private final PrintWriter writer;
	private final BufferedReader reader;
	
	private final Gson gson;
//	private final GameManeger gManeger;
	
//l	private final LinkedList<String> msgList;

//	public CommunicationDelegator(String host, int port) throws UnknownHostException, IOException{
//		socket = new Socket(host, port);
//
//		writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
//        reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
//        
//        msgList = new LinkedList<String>();
//	}
	
	public CommunicationDelegator(PrintWriter writer, BufferedReader reader){
		this.writer = writer;
		this.reader = reader;
		this.gson = new Gson();

//		// test
//        send(new Join("y-lab","fgVPJS97TJ7F/A"));        
//        String line;
//        try {
//			while((line = reader.readLine()) != null) {
//				System.out.println(line);
//			    final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
//			    if (msgFromServer.msgType.equals("carPositions")) {
//			        send(new communication.msg.sendmsg.Throttle(0.6));
//			    } else if (msgFromServer.msgType.equals("join")) {
//			        System.out.println("Joined");
//			    } else if (msgFromServer.msgType.equals("gameInit")) {
//			        System.out.println("Race init");
//			    } else if (msgFromServer.msgType.equals("gameEnd")) {
//			        System.out.println("Race end");
//			    } else if (msgFromServer.msgType.equals("gameStart")) {
//			        System.out.println("Race start");
//			    } else {
//			        send(new communication.msg.sendmsg.Ping());
//			    }
//			}
//		} catch (JsonSyntaxException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
////		test end;
	}
	
   public void send(final SendMsg msg) {
	   String str = msg.toJson();
	   System.out.println(str);
	   writer.println(str);
	   writer.flush();
   }
   
   public RecieveMsg readMsg() throws IOException, UnknownMsgException {
	   String msg = reader.readLine();
	   System.out.println("RecieveMsg : " + msg);

	   return this.gson.fromJson(msg, RecieveMsg.class);
	   
//	   return this.MsgWrapper2RecieveMsg(rmw);
//	   return new RecieveMsg(rmw);
   }
   
//   private RecieveMsg MsgWrapper2RecieveMsg(RecieveMsgWrapper rMsgWppr) throws UnknownMsgException{
//	   String msgType = rMsgWppr.getMsgType();
//	   if(msgType.equals("yourCar")){
//		   return new RecieveMsg(rMsgWppr);
//	   }else if(msgType.equals("gameInit")){
//		   return new RecieveMsg(msgType, this.gson.fromJson(rMsgWppr.data.toString(), GameInit.class), rMsgWppr.gameId, rMsgWppr.gameTick);
//	   }else if(msgType.equals("carPositions")){
//		   return new RecieveMsg(msgType, this.gson.fromJson(rMsgWppr.data.toString(), CarPosition.class), rMsgWppr.gameId, rMsgWppr.gameTick);
//	   }else if(msgType.equals("gameEnd")){
//		   return new RecieveMsg(msgType, this.gson.fromJson(rMsgWppr.data.toString(), GameEnd.class), rMsgWppr.gameId, rMsgWppr.gameTick);
//	   }else if(msgType.equals("crash")){
//		   return new RecieveMsg(msgType, this.gson.fromJson(rMsgWppr.data.toString(), Crash.class), rMsgWppr.gameId, rMsgWppr.gameTick);
//	   }else if(msgType.equals("spawn")){
//		   return new RecieveMsg(msgType, this.gson.fromJson(rMsgWppr.data.toString(), Spawn.class), rMsgWppr.gameId, rMsgWppr.gameTick);
//	   }else if(msgType.equals("lapFinished")){
//		   return new RecieveMsg(msgType, this.gson.fromJson(rMsgWppr.data.toString(), LapFinished.class), rMsgWppr.gameId, rMsgWppr.gameTick);
//	   }else if(msgType.equals("dnf")){
//		   return new RecieveMsg(msgType, this.gson.fromJson(rMsgWppr.data.toString(), Dnf.class), rMsgWppr.gameId, rMsgWppr.gameTick);
//	   }else if(msgType.equals("finish")){
//		   return new RecieveMsg(msgType, this.gson.fromJson(rMsgWppr.data.toString(), Finish.class), rMsgWppr.gameId, rMsgWppr.gameTick);
//	   }else if( msgType.equals("gameStart") || msgType.equals("tornamentEnd") ){
//		   return new RecieveMsg(msgType, rMsgWppr.data, rMsgWppr.gameId, rMsgWppr.gameTick);		   
//	   }
//	   throw new UnknownMsgException();
//   }
   
   public void sendJoin(String botName, String botKey) {
	   send(new Join(botName, botKey));
	   //send(new JoinRace(botName, botKey, "U.S.A.", null, 1));
//	   String trackName = "usa";
//	   String trackName = "germany";
//	   String trackName = "france";
//	   String str = "{\"msgType\": \"joinRace\", \"data\": {\"botId\": {\"name\": \""+botName+"\", \"key\": \""+botKey+"\"},\"trackName\": \""+trackName+"\", \"carCount\": " + 1 + "}}";
//	   System.out.println(str);
//	   writer.println(str);
//	   writer.flush();

   }

   public void sendSR() {
	   String str = "{\"msgType\": \"switchLane\", \"data\": \"Right\"}";
	   System.out.println(str);
	   writer.println(str);
	   writer.flush();
   }
   public void sendSL() {
	   String str = "{\"msgType\": \"switchLane\", \"data\": \"Left\"}";
	   System.out.println(str);
	   writer.println(str);
	   writer.flush();
   }

//   public void tmp(){
//	   String serverMsg = null;
//	   try {
//			serverMsg = reader.readLine();
//	   } catch (IOException e1) {
//		   
//		   e1.printStackTrace();
//	   }
//	   while((serverMsg) != null){
//		   System.out.println("get! :" + serverMsg);
//
//		   final MsgWrapper msgFromServer = (new Gson()).fromJson(serverMsg, MsgWrapper.class);
//		   if (msgFromServer.msgType.equals("carPositions")) {
//			   //                delegator.send(new Throttle(0.6));
//		   } else if (msgFromServer.msgType.equals("join")) {
//	//		   System.out.println("Joined");
//		   } else if (msgFromServer.msgType.equals("gameInit")) {
////			   System.out.println("Race init");
//		   } else if (msgFromServer.msgType.equals("gameEnd")) {
////			   System.out.println("Race end");
//		   } else if (msgFromServer.msgType.equals("gameStart")) {
////			   System.out.println("Race start");
//		   } else {
//			   //              delegator.send(new Ping());
//		   }
//		   synchronized(this){
//			   msgList.add(serverMsg);
//		   };
//		   try {
//			   serverMsg = reader.readLine();
//		   } catch (IOException e) {
//			   e.printStackTrace();
//		   }
//	   }
//   }

//	   try {
//		  return reader.readLine();
//	   } catch (IOException e) {
//		   e.printStackTrace();
//		   return null;
//	   }
	
//	enum Msg{
//		Join,
//	}
	
}
