package communication.jsondata;

import gamedata.PiecePosition;

public class CarPosition {

	private ID id;
	private float angle;
	private PiecePosition piecePosition;
	
	public String getName(){
		return id.name;
	}
	
	public String getColor(){
		return id.color;
	}
	
	public float getAngle(){
		return angle;
	}
	
	public PiecePosition getPeicePosition(){
		return piecePosition;
	}
		
}

class ID{
	String name;
	String color;
}