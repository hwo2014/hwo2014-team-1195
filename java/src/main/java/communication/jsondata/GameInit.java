package communication.jsondata;

import gamedata.Car;
import gamedata.Track;

import java.util.ArrayList;


public class GameInit {
	private Race race;

	public Track getTrack(){
		return race.track;
	}

	public String getCarColor(String botName){
		CarSpec spec = getCarSpec(botName);
		if(spec != null){
			return spec.id.color;
		}
		
		return null;
	}

	public float getCarLength(String botName){
		CarSpec spec = getCarSpec(botName);
		if(spec != null){
			return spec.dimensions.length;
		}
		
		return -1;
	}

	public float getCarWidth(String botName){
		CarSpec spec = getCarSpec(botName);
		if(spec != null){
			return spec.dimensions.width;
		}
		
		return -1;
	}

	public float getCarGuideFlagPosition(String botName){
		CarSpec spec = getCarSpec(botName);
		if(spec != null){
			return spec.dimensions.guideFlagPosition;
		}
		
		return -1;
	}
	
	public CarSpec getCarSpec(String botName){
		CarSpec[] cars = race.cars;
		for(int i=0; i<cars.length; i++){
			String carName = cars[i].id.name;
			if(carName.equals(botName)){
				return cars[i];
			}
		}
		
		return null;
	}
	
	public Car[] getEnemyCar(String botName){
		ArrayList<Car> ret = new ArrayList<Car>();
		for(CarSpec cs : race.cars){
			if(cs.id.name != botName){
				ret.add(new Car(cs.id.name, this));				
			}
		}
		return ret.toArray(new Car[0]);
	}

	public RaceSession getRaceSession(){
		return race.raceSession;
	}
}

class Race {
	public Track track;
	public CarSpec[] cars;
	public RaceSession raceSession;	
}

class CarSpec{
	ID id;
	Dimensions dimensions;
	
	class ID{
		String name;
		String color;
	}
	
	class Dimensions{
		float length;
		float width;
		float guideFlagPosition;
	}
}

