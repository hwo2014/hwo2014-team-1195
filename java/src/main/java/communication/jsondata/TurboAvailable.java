package communication.jsondata;

public class TurboAvailable {
	private double turboDurationMilliseconds;
	private double turboDurationTicks;
	private double turboFactor;
	
	public double getTurboDurationMilliseconds() {
		return turboDurationMilliseconds;
	}
	public double getTurboDurationTicks() {
		return turboDurationTicks;
	}
	public double getTurboFactor() {
		return turboFactor;
	}
	
}
