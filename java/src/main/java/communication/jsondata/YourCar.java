package communication.jsondata;

public class YourCar {
	private String name;
	private String color;
	
	public String getName() {
		return new String(name);
	}
	
	public String getColor() {
		return new String(color);
	}
	
}
