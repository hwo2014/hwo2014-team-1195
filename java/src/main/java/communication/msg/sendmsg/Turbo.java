package communication.msg.sendmsg;

import com.google.gson.Gson;

import communication.msg.MsgWrapper;
import communication.msg.SendMsg;

public class Turbo extends SendMsg {
	
	@Override
	public String toJson() {
		// TODO 自動生成されたメソッド・スタブ
		return new Gson().toJson(new MsgWrapper(this.msgType(), "Wooooo Hooooooo!!!!"));
	}

	@Override
	protected String msgType() {
		// TODO 自動生成されたメソッド・スタブ
		return "turbo";
	}

}
