package communication.msg.sendmsg;

import communication.msg.SendMsg;

public class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}