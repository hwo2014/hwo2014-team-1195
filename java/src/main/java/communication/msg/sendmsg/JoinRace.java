package communication.msg.sendmsg;

import com.google.gson.Gson;

import communication.msg.MsgWrapper;
import communication.msg.SendMsg;

public class JoinRace extends SendMsg{
	Object d;
	public JoinRace(final String botname, final String botkey, final String trackname, final String password, final int carcount){
		d = new Data(botname, botkey, trackname);
//		if(trackname == null){
//			d = new Object(){BotId botId = new BotId(botname,botkey);};
//		}else if(password == null){
//			d = new Object(){BotId botId = new BotId(botname,botkey); String trackName = trackname; int carCount = carcount;};
//		}else{
//			final String p = password;
//			d = new Object(){BotId botId = new BotId(botname,botkey); String trackName = trackname; int carCount = carcount; String password = p;};
//		}
	}

	@Override
	public String toJson() {
		// TODO 自動生成されたメソッド・スタブ
		return new Gson().toJson(new MsgWrapper(this.msgType(), d));
	}

	@Override
	protected String msgType() {
		// TODO 自動生成されたメソッド・スタブ
		return "joinRace";
	}
	
	class Data{
		BotId botId;
		String trackName;
		int carCount = 1;
		public Data(String botName, String botkey, String trackName){
			botId = new BotId(botName, botkey);
			this.trackName = trackName;
		}
	}
	
	class BotId{
		String name;
		String key;
		public BotId(String name, String key){
			this.name = name;
			this.key = key;
		}
	}

}
