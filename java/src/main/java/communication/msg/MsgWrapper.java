package communication.msg;



public class MsgWrapper {
    public final String msgType;
    public final Object data;

    public MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    protected MsgWrapper(final SendMsg smsg) {
        this(smsg.msgType(), smsg.msgData());
    }
}