package communication.msg;


@Deprecated
public class RecieveMsgWrapper {
    private String msgType;
    private Object data;
    private String gameId;
    private int gameTick = -1;

//    protected RecieveMsgWrapper(final String msgType, final Object data) {
//        this.msgType = msgType;
//        this.data = data;
//        this.gameId = null;
//        this.gameTick = -1;
//    }

    public String getMsgType() {
		return msgType;
	}

	public Object getData() {
		return data;
	}

	public String getGameId() {
		return gameId;
	}

	public int getGameTick() {
		return gameTick;
	}
	
//	protected RecieveMsgWrapper(final RecieveMsg rmsg) {
//        this(rmsg.getMsgType(), rmsg.getData());
//    }
}
