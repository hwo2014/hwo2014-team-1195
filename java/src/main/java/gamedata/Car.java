package gamedata;

import java.util.LinkedList;

import communication.jsondata.CarPosition;
import communication.jsondata.GameInit;

/**
 * @author can8anyone4hear8me1
 *
 */
public class Car {

	private final String name;
	private final String color;
	private final float length;
	private final float width;
	private final float guideFlagPosition;

	private final LinkedList<CarState> carStateList;
	
	private Object turboData;
	
	public Car(String botName, GameInit gameInit){
		this.name = botName;
		this.color = gameInit.getCarColor(botName);
		this.length = gameInit.getCarLength(botName);
		this.width = gameInit.getCarWidth(botName);
		this.guideFlagPosition = gameInit.getCarGuideFlagPosition(botName);
		this.carStateList = new LinkedList<CarState>();
	}
	
	public void setState(CarPosition carPosition, int gameTick){
		carStateList.addFirst(new CarState(carPosition, gameTick));
	}
	
	public String getName() {
		return name;
	}
	public String getColor() {
		return color;
	}
	public float getLength() {
		return length;
	}
	public float getWidth() {
		return width;
	}
	public float getGuideFlagPosition() {
		return guideFlagPosition;
	}
	
	public CarState getCarState(){
		return this.getCarState(0);
	}

	/**
	 * @param index
	 * @return
	 */
	public CarState getCarState(int index) throws IndexOutOfBoundsException{
		return carStateList.get(index);
	}

	public float getAngle() {
		return this.getAngle(0);
	}
	
	/**
	 * @param index
	 * @return
	 */
	public float getAngle(int index) throws IndexOutOfBoundsException{
		return carStateList.get(index).getAngle();
	}
		
	public PiecePosition getPiecePosition() {
		return carStateList.peek().getPeicePosition();
	}
	
	public PiecePosition getPeicePosition(int index) throws IndexOutOfBoundsException{
		return carStateList.get(index).getPeicePosition();
	}
	
	public void setTurboData(Object data){
		this.turboData = data;
	}
	
	public Object getTurboData(){
		return this.turboData;
	}
	
}



