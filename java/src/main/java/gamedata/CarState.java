package gamedata;

import communication.jsondata.CarPosition;

public class CarState{
	private final float angle;
	private final PiecePosition piecePosition;
	private final int gameTick;
	
	public CarState(CarPosition carPosition, int gameTick){
		this.angle = carPosition.getAngle();
		this.piecePosition = carPosition.getPeicePosition();
		this.gameTick = gameTick;
	}

	public float getAngle() {
		return angle;
	}

	public PiecePosition getPeicePosition() {
		return piecePosition;
	}
	
	public int getGameTick(){
		return gameTick;
	}
}
