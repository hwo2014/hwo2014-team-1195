package gamedata;

public class PiecePosition{
	private int pieceIndex;
	private float inPieceDistance;
	private Lane lane;
	private int lap;
	
	public PiecePosition(int pieceIndex, float inPieceDistance, int startLaneIndex, int endLaneIndex, int lap){
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lane = new Lane();
		this.lane.startLaneIndex = startLaneIndex;
		this.lane.endLaneIndex = endLaneIndex;
		this.lap = lap;
	}
	
	public int getPieceIndex() {
		return pieceIndex;
	}

	public float getInPeiceDistance() {
		return inPieceDistance;
	}
	
	public int getLap(){
		return lap;
	}

	public int getStartLaneIndex(){
		return lane.startLaneIndex;
	}
	
	public int getEndLaneIndex(){
		return lane.endLaneIndex;
	}

	class Lane{
		public int startLaneIndex;
		public int endLaneIndex;
	}
}
