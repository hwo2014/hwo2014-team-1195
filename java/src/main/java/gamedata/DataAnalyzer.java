package gamedata;

import gamedata.Track.Lane;

public class DataAnalyzer {
	/*
	 * @param gData
	 * @param p0
	 * @param p1
	 * @return
	 */
//	public static double getDist(GameData gData, PiecePosition p0, PiecePosition p1){
//		float p1Length = p1.getLap() * gData.getTrackLength() + gData.getPieceDist(p1.getPieceIndex()) + p1.getInPeiceDistance();
//		float p0Length = p0.getLap() * gData.getTrackLength() + gData.getPieceDist(p0.getPieceIndex()) + p0.getInPeiceDistance();
//		
//		return p1Length - p0Length;
//	}
	
	public static double getSpead(final GameData gData, final Car car, final int index) throws IndexOutOfBoundsException{
		CarState curt = car.getCarState(index);
		CarState prev = car.getCarState(index+1);
		
		double dist = getDist(gData, prev.getPeicePosition(), curt.getPeicePosition());
		int dt = curt.getGameTick() - prev.getGameTick();
		
		return dist/dt;
	}
	
	public static double getSpead(GameData gData, Car car){
		try{
			return getSpead(gData, car, 0);
		}catch(IndexOutOfBoundsException e){
			return 0;
		}
	}
	
	public static double getNextTurnDist(GameData gData, Car car){
		PiecePosition pos = car.getPiecePosition();		
		int i = (pos.getPieceIndex() + 1) % gData.getTrack().pieces.length;
		while(i != pos.getPieceIndex()){
			Piece piece = gData.getTrack().pieces[i];
			if(piece.isTurn()){
				return getDist(gData, car.getPeicePosition(0), new PiecePosition(i,0,pos.getStartLaneIndex(), pos.getEndLaneIndex(),pos.getLap()));
			}
			i = (i+1)%gData.getTrack().pieces.length;
		}
		return 100000;
	}
	
	public static Piece getNextTurn(GameData gData, PiecePosition piecePosition){
		PiecePosition pos = piecePosition;
		int i = (pos.getPieceIndex() + 1) % gData.getTrack().pieces.length;
		while(i != pos.getPieceIndex()){
			Piece piece = gData.getTrack().pieces[i];
			if(piece.isTurn()){
				return piece;
			}
			i = (i+1)%gData.getTrack().pieces.length;
		}
		return null;
	}
	
	/*
	 * @param gData
	 * @param p0
	 * @param p1
	 * @return
	 */
	public static double getDist(GameData gData, PiecePosition p0, PiecePosition p1){
		PiecePosition pf;
		PiecePosition pb;
		
		if(p1.getLap() == p0.getLap()){
			if(p1.getPieceIndex() == p0.getPieceIndex()){
				if(p1.getInPeiceDistance() > p0.getInPeiceDistance()){
					pf = p1;
					pb = p0;
				}else if(p1.getInPeiceDistance() < p0.getInPeiceDistance()){
					pf = p0;
					pb = p1;
				}else{
					return 0;
				}				
			}else if(p1.getPieceIndex() > p0.getPieceIndex()){
				pf = p1;
				pb = p0;
			}else{
				pf = p0;
				pb = p1;
			}
		}else if(p1.getLap() > p0.getLap()){
			pf = p1;
			pb = p0;
		}else{
			pf = p0;
			pb = p1;
		}
		
		float distf = - pb.getInPeiceDistance();
		float distb = - pb.getInPeiceDistance();
		Lane lanef = gData.getTrack().getLane(pf.getStartLaneIndex());
		Lane laneb = gData.getTrack().getLane(pb.getEndLaneIndex());
		
		int i = pb.getLap();
		int j = pb.getPieceIndex();
		while((i <= pf.getLap() && j != pf.getPieceIndex())){
			Piece p = gData.getTrack().getPiece(j);
			distf += p.getLength(lanef);
			distb += p.getLength(laneb);
			
			j = (j+1) % gData.getTrack().getPieces().length;
			if(j == pf.getPieceIndex()){
				if(i < pf.getLap()){
					i++;
				}else{
					break;
				}
			}
		}
		distf += pf.getInPeiceDistance();
		distb += pf.getInPeiceDistance();
		if(p0 == pb){
			return (distf + distb)/2;
		}else{
			return -(distf + distb)/2;
		}
		
//		final int p1Index = p1.getPieceIndex();
//		final int p1Lap = p1.getLap();
//		final int p0Index = p0.getPieceIndex();
//		final int p0Lap = p0.getLap();
//		
//		float dist = - p0.getInPeiceDistance();
//		Lane nowLane = gData.getTrack().getLane(p1.getStartLaneIndex());
//		Lane oldLane = gData.getTrack().getLane(p0.getEndLaneIndex());
//
//		for(int i = p0Index; i != p1Index; i = (i+1) % gData.getTrack().getPieces().length){
//			Piece p = gData.getTrack().getPiece(i);
//			if(!p.isTurn()){
//				dist += p.getLength();
//			}else{
//				dist += p.getRadius() * Math.PI * Math.abs(p.getAngle()/180);
//
//				dist -= nowLane.distanceFromCenter * Math.PI * p.getAngle()/180;
//			}
//		}
//		dist += p1.getInPeiceDistance();
//		return dist;			
	}

}
