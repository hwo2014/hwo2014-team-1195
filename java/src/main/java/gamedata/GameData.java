package gamedata;

import communication.jsondata.CarPosition;
import communication.jsondata.GameInit;
import communication.jsondata.RaceSession;
import communication.jsondata.TurboAvailable;
import communication.jsondata.TurboEnd;
import communication.jsondata.TurboStart;


public class GameData {

	private final Track track;
	private final Car myCar;
	private final Car[] enemyCars;
	
	private final RaceSession raceSession;
	
	private final float[] pieceDist;
	
	public GameData(String botName, GameInit gameInit){
		this.track = gameInit.getTrack();
		this.myCar = new Car(botName, gameInit);
		this.raceSession = gameInit.getRaceSession();
		this.enemyCars = gameInit.getEnemyCar(botName);
		this.pieceDist = culcPieceDist(this.track);
	}
		
	private float[] culcPieceDist(Track t) {
		float[] pieceDist = new float[t.getPieces().length+1];
		pieceDist[0] = 0;
		for(int i = 1; i < pieceDist.length; i++){
			pieceDist[i] = pieceDist[i-1] + t.getPiece(i-1).getLength();
		}
		return pieceDist;
	}

	public Track getTrack() {
		return track;
	}

	public Car getMyCar() {
		return myCar;
	}
	
	public RaceSession getRaceSession(){
		return raceSession;
	}
	
	public void update(CarPosition[] carPositions, int gameTick){
		for(CarPosition pos : carPositions){
			String name = pos.getName();
			if(name.equals(myCar.getName())){
				myCar.setState(pos, gameTick);
				continue;
			}
			for(Car car : enemyCars){
				if(name == car.getName()){
					car.setState(pos, gameTick);
					break;
				}
			}
		}
	}
	
	public void update(TurboAvailable ta, int gameTick){
		this.myCar.setTurboData(ta);
	}
	public void update(TurboStart ts, int gameTick) {
		String name = ts.getName();
		if(myCar.getName().equals(name)){
			myCar.setTurboData(null);
		}else{
			for(Car c : this.enemyCars){
				if(c.getName().equals(name)){
					c.setTurboData(null);
					return;
				}
			}
		}
	}
	public void update(TurboEnd te, int gameTick){
		//
	}
	
	public float[] getPieceDist(){
		return this.pieceDist;
	}
	
	public float getPieceDist(int index){
		return this.pieceDist[index];
	}
	
	public float getTrackLength(){
		return this.pieceDist[pieceDist.length-1];
	}

}
