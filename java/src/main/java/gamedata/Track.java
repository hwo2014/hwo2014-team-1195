package gamedata;

import java.awt.Point;

public class Track {
	public String id;
	public String name;
	public Piece[] pieces;
	public Lane[] lanes;
	public StartingPoint startingPoint;
	
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public Piece getPiece(int index){
		return pieces[index%pieces.length];
	}
	
	public Piece[] getPieces(){
		return pieces;
	}
	
	public int getPieceIndex(Piece p){
		for(int i = 0; i<pieces.length; i++){
			if(pieces[i] == p){
				return i;
			}
		}
		return 0;
	}
	
	public Lane getLane(int index){
		for(Lane l : lanes){
			if(l.index == index) return l;
		}
		return null;
	}
		
	class Lane{
		int distanceFromCenter;
		int index;
	}
	
	class StartingPoint{
		Point position;
		float angle;		
	}
}

