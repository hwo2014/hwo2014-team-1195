package gamedata;

import gamedata.Track.Lane;

import com.google.gson.annotations.SerializedName;

public class Piece{
	private float length = 0;
	private float radius = 0;
	private float angle = 0;
	
	@SerializedName("switch")
	private boolean isSwitch = false;

	/**
	 * @return
	 */
	public float getLength() {
		if(this.isTurn()){
			return (float) (Math.PI * radius * Math.abs(angle/180));
		}
		return length;
	}
	
	public float getLength(Lane l){
		if(this.isTurn()){
			float ans = 0;
			ans = (float) Math.PI * ((radius * Math.abs(angle/180)) - (l.distanceFromCenter * (angle/180)));
			return ans;
		}
		return length;
	}
	
	/**
	 * @return
	 */
	public float getVariableLength(){
		return length;
	}
	
	public float getRadius(){
		return radius;
	}

	public float getAngle() {
		return angle;
	}

	public boolean isSwitch() {
		return isSwitch;
	}
	
	public boolean isTurn(){
		return (radius != 0);
	}
	
}
