package ylabbot;

import gamedata.Car;
import gamedata.DataAnalyzer;
import gamedata.GameData;
import gamedata.Piece;
import gamedata.PiecePosition;
import communication.msg.SendMsg;
import communication.msg.sendmsg.Throttle;
import communication.msg.sendmsg.Turbo;

public class YlabBot {
	
//	private final CommunicationDelegator delegator;
	
	private final String name;
	private final String color;
	
	public YlabBot(String botName, String botColor){
		this.name = botName;
		this.color = botColor;
//		delegator = dlgtr;
		
//		this.botName = name;
//		this.key = key;
	}
	
	public String getName(){
		return new String(this.name);
	}
	
	public String getColor(){
		return new String(this.color);
	}
	
	public SendMsg getMsg(GameData data){
		Car myCar = data.getMyCar();
//		double throttle = 0.8 - Math.pow(Math.abs(myCar.getAngle())/60.0,0.4);
//		double throttle = 0.8 - Math.abs(myCar.getAngle())/10;
		double spead = DataAnalyzer.getSpead(data, myCar);
		double throttle = 0.0;
//		double nextTurnDist = DataAnalyzer.getNextTurnDist(data, myCar);
		Piece nextTurn = DataAnalyzer.getNextTurn(data, myCar.getPiecePosition());
		Piece tmp = nextTurn;
		Piece nowPiece = data.getTrack().getPiece(myCar.getPiecePosition().getPieceIndex());
//		while(nextTurn.getRadius() > 300 || ((nowPiece.getRadius()-5) < nextTurn.getRadius() && nextTurn.getRadius() < (nowPiece.getRadius()) + 5)){
		while(((nowPiece.getRadius()-5) < nextTurn.getRadius() && nextTurn.getRadius() < (nowPiece.getRadius()) + 5) && Math.signum(nowPiece.getAngle()) == Math.signum(nextTurn.getAngle())){
			nextTurn = DataAnalyzer.getNextTurn(data, new PiecePosition(data.getTrack().getPieceIndex(nextTurn), 0, 0, myCar.getPiecePosition().getEndLaneIndex(), myCar.getPiecePosition().getLap()));
			if(nextTurn == tmp){
				break;
			}
		}
		int lap = myCar.getPiecePosition().getLap();
		if(myCar.getPiecePosition().getPieceIndex() > data.getTrack().getPieceIndex(nextTurn)){
			lap++;
		}
		PiecePosition turnP = new PiecePosition(data.getTrack().getPieceIndex(nextTurn), 0, 0, myCar.getPiecePosition().getEndLaneIndex(), lap);
		double nextTurnDist = DataAnalyzer.getDist(data, myCar.getPiecePosition(), turnP);
		
		if(myCar.getTurboData() != null){
//			if(nextTurn.getRadius() > 200){
//				return new Turbo();
//			}else{}
			Piece nextPiece = data.getTrack().getPiece((myCar.getPiecePosition().getPieceIndex() + 1));
			if((nextTurnDist > 600) && !nextPiece.isTurn() && Math.abs(myCar.getAngle())<10){
				return new Turbo();
			}
		}
		if(spead < 12){
			throttle = 0;
		}
		if(nextTurnDist < 100 && spead > Math.abs(nextTurn.getRadius())/20.0){
			double sLimit0 = Math.pow(nextTurn.getRadius(),2)/2000.0;
			if(spead > sLimit0 || nextTurnDist < 50){
				throttle = 0;
			}else{
				throttle = 0.5;
			}
		}else{
			throttle = (15.0 / (Math.abs(myCar.getAngle()) + 15.0));
		}
//		double throttle = Math.min((2.0/(spead+0.01)),1) * (2.0 / (Math.abs(myCar.getAngle()) + 2.0));
		System.out.println("Spead : " + spead + " Throttle : " + throttle);
		SendMsg msg = new Throttle(throttle);
		return msg;
	}
	
//	public void getName
	
//	public void start(){
//		
////		delegator.send(new Join(botName, key));
//		
//		new Thread(){
//			@Override
//			public void run(){
//				delegator.tmp();
//			}
//		}.start();
//		
//		String serverMsg;
////		while((serverMsg = delegator.read()) != null){
//		while(true){
//			serverMsg = delegator.read();
//			if(serverMsg == null) continue;
//			System.out.println(serverMsg);
//            final MsgWrapper msgFromServer = (new Gson()).fromJson(serverMsg, MsgWrapper.class);
//            if (msgFromServer.msgType.equals("carPositions")) {
//                delegator.send(new Throttle(0.6));
//            } else if (msgFromServer.msgType.equals("join")) {
//                System.out.println("Joined");
//            } else if (msgFromServer.msgType.equals("gameInit")) {
//                System.out.println("Race init");
//            } else if (msgFromServer.msgType.equals("gameEnd")) {
//                System.out.println("Race end");
//                break;
//            } else if (msgFromServer.msgType.equals("gameStart")) {
//                System.out.println("Race start");
//            } else {
//                delegator.send(new Ping());
//            }
//            try {
//            	synchronized(this){
//            		this.wait(2000);
//            	};
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
//	}
	
}
