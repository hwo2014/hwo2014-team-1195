package ylabbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Main {
    public static void main(String... args) throws UnknownHostException, IOException{
        final String host = args[0];
        final int port = Integer.parseInt(args[1]);
        final String botName = args[2];
        final String botKey = args[3];


//        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
//        try{
//        	delegator = new CommunicationDelegator(host, port);
//        }catch(IOException e){
//        	e.setStackTrace(e.getStackTrace());
//        	return;
//        }
        
//        YlabBot ybot = new YlabBot(delegator, botName, botKey);
        
        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        
//        try ( Socket s = new Socket(host, port) ){
//         	final PrintWriter w = new PrintWriter(new OutputStreamWriter(s.getOutputStream(), "utf-8"));
//        	final BufferedReader r = new BufferedReader(new InputStreamReader(s.getInputStream(), "utf-8"));
//
//        	GameManeger gmane = new GameManeger(w,r);
//        	gmane.startNewGame(botName, botKey);
//        } catch (IOException e1) {
//        	// TODO 自動生成された catch ブロック
//        	e1.printStackTrace();
//        }
        
        Socket s = new Socket(host, port);
     	final PrintWriter w = new PrintWriter(new OutputStreamWriter(s.getOutputStream(), "utf-8"));
    	final BufferedReader r = new BufferedReader(new InputStreamReader(s.getInputStream(), "utf-8"));

    	GameManeger gmane = new GameManeger(w,r);
    	gmane.startNewGame(botName, botKey);
        
    	s.close();
    }

//    final Gson gson = new Gson();
//    private PrintWriter writer;
//
//    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
//        this.writer = writer;
//        String line = null;
//
//        this.send(new Ping());
//        send(join);
//
//        while((line = reader.readLine()) != null) {
//            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
//            if (msgFromServer.msgType.equals("carPositions")) {
//                send(new Throttle(0.6));
//            } else if (msgFromServer.msgType.equals("join")) {
//                System.out.println("Joined");
//            } else if (msgFromServer.msgType.equals("gameInit")) {
//                System.out.println("Race init");
//            } else if (msgFromServer.msgType.equals("gameEnd")) {
//                System.out.println("Race end");
//            } else if (msgFromServer.msgType.equals("gameStart")) {
//                System.out.println("Race start");
//            } else {
//                send(new Ping());
//            }
//        }
//    }
//
//    private void send(final SendMsg msg) {
//        writer.println(msg.toJson());
//        writer.flush();
//    }
}
