package ylabbot;

import gamedata.GameData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import com.google.gson.Gson;
import communication.CommunicationDelegator;
import communication.RecieveMsg;
import communication.UnknownMsgException;
import communication.jsondata.CarPosition;
import communication.jsondata.GameInit;
import communication.jsondata.TurboAvailable;
import communication.jsondata.TurboEnd;
import communication.jsondata.TurboStart;
import communication.jsondata.YourCar;
import communication.msg.sendmsg.Throttle;

public class GameManeger {
	private final PrintWriter writer;
	private final BufferedReader reader;
	
//	private final Gson gson;
	
	private CommunicationDelegator delegator;
	private YlabBot ybot;
	private GameData gData;
//	private String botName;
//	private String botKey;
	
	public GameManeger(final PrintWriter writer, final BufferedReader reader) {
		this.writer = writer;
		this.reader = reader;
//		this.gson = 
	}
	
	public void startNewGame(String botName, String botKey) throws IOException{
//		this.botName = botName;
//		this.botKey = botKey;
		this.delegator = new CommunicationDelegator(writer, reader);
//		this.ybot = new YlabBot(botName, botKey, this.delegator);

		delegator.sendJoin(botName, botKey);
		
		String botColor = "";
		Gson gson = new Gson();
		
		while(true){
			RecieveMsg rMsg;
			try {
				rMsg = delegator.readMsg();
			} catch (UnknownMsgException e) {
				e.printStackTrace();
				continue;
			}
			if(rMsg.getMsgType().equals("gameInit")){
				this.gData = new GameData(botName, gson.fromJson(rMsg.getData().toString(), GameInit.class));
			}else if(rMsg.equals("yourCar")){
				YourCar yc = gson.fromJson(rMsg.getData().toString(), YourCar.class);
				botName = yc.getName();
				botColor = yc.getColor();
				this.ybot = new YlabBot(botName, botColor);
			}else if(rMsg.equals("carPositions")){
				this.gData.update(gson.fromJson(rMsg.getData().toString(), CarPosition[].class), rMsg.getGameTick());
			}else if(rMsg.equals("join")){
				continue;
			}else if(rMsg.equals("joinRace")){
				continue;
			}else if(rMsg.equals("gameStart")){
				delegator.send(ybot.getMsg(gData));
				break;
			}else{
				System.out.println("Can not start game!");
				//break;
			}
		}
		
		this.gameLoop();
	}
	
	private void gameLoop() throws IOException{
		Gson gson = new Gson();
		
		while(true){
			RecieveMsg rMsg;
			try {
				rMsg = delegator.readMsg();
			} catch (UnknownMsgException e) {
				e.printStackTrace();
				continue;
			}
			if(rMsg == null){
				break;
			}else if(rMsg.getMsgType().equals("carPositions")){
//				CarPosition[] hoge = gson.fromJson(rMsg.getData().toString(), CarPosition[].class);
				gData.update(gson.fromJson(rMsg.getData().toString(), CarPosition[].class), rMsg.getGameTick());
				if(rMsg.getGameTick() % 1000 == 0){
					if(rMsg.getGameTick() % 500 == 0){
						delegator.sendSR();
					}else{
						delegator.sendSL();
					}
				}
				delegator.send(ybot.getMsg(gData));
			}else if(rMsg.getMsgType().equals("crash")){
				
			}else if(rMsg.getMsgType().equals("spawn")){
				
			}else if(rMsg.getMsgType().equals("lapFinished")){
				
			}else if(rMsg.getMsgType().equals("turboAvailable")){
				gData.update(gson.fromJson(rMsg.getData().toString(), TurboAvailable.class), rMsg.getGameTick());
			}else if(rMsg.getMsgType().equals("turboStart")){
				gData.update(gson.fromJson(rMsg.getData().toString(), TurboStart.class), rMsg.getGameTick());
			}else if(rMsg.getMsgType().equals("turboEnd")){				
				gData.update(gson.fromJson(rMsg.getData().toString(), TurboEnd.class), rMsg.getGameTick());
			}else if(rMsg.getMsgType().equals("dnf")){
				
			}else if(rMsg.getMsgType().equals("finish")){

			}else if(rMsg.getMsgType().equals("gameEnd")){
//				this.finishGame(rMsg);
//				break;
			}else{
				System.out.println("Unknow Message! : " + rMsg.getMsgType());
				delegator.send(new Throttle(1.0));
			}

		}
	}

	private void finishGame(RecieveMsg endMsg) {
		if(!endMsg.equals("gameEnd")){
			throw new IllegalArgumentException();
		}
		
	}
	
}
